# API Management / Access Management

---

## Api Management ?

* Créer et publier des APis,
* Gérer  les règles d'usage et contrôler les accès,
* Gérer l'aspect communautaire,
* Collecter et analyser les usages.

---

## Basics - 1. Documentation

* How the API is working
* don't waste time
* Try-i mode

---

## Basics - 2. Analytics
* Understand your consumers
* How your API is consumed

---

## Basics - 3. Fl"xibility

* On premise
* Cloud
* Hybrid

---

## Basics - 4. Developer engagement

* Portal
* On-Boarding

Note: Favoriser le developpeur

---

## Basics - 5. SandBox

* Improve Adoption
* Dedicated environment

Note: 
* Pouvoir tester directement les apis et voir le résultat
* Eviter de remonter des données de Production

---

## Basics - 6 .Traffic Management

* Performance
* Routing
* Caching

--- 

## Basics - 7. Security

* Sensible data
* Identity and access Management
* Users and developers

---

## Basics - 8. Monetize

* Make money with your APis
* Must not be an end

---

## Basics - 9. Availiabilty

* HA
* Scalable
* Resiliency

Note: Assurer une haute dispo (SLA), Scalabilit pour les pics de charge, ....

---

## Basics - 10. Support Legacy systems

* Existing softwares
* SOAP
* ...

---

## Lifecycle

--- 

- API Provider
* Strategy/Design
* Create
* Test/Publish
* Secure
* Manage

- API Consumer
* Discover
* Develop
* Consumer

-> Provider : Optimize
-> Consumer : Monitor

---

## Components

---

* API Portal
* API Gateway

---

### API Portal

* Documentation
* Registration
* Analysis

### API Gateway
* Security
* ...

--- 

## Which API

* Public
* Private
* Partner

--- 

### Developer Experience

---

* Design/Documentation
* Developer Portal

Note: 
Documentation: Consistance dans le designe et la documentation
Developer Portal: 
Portail accessible facilement et centralisé
Documentation fonctionnelle
Try-it
Support

---

* Think your API as a product.
* Eat your own dog's food.
* Keep the developer in mind.
* Developers are people too!

Note: 
Déveloper les api comme des produits pour toujours penser à l'utilisation
Utiliser nous-même les api

---

## API Management to beautify mi APIs ?

---

## API Management to beautify mi APIs ?
# NO

--- 

## API management to expopse my APIs ?

--- 

## API management to expose my APIs ?
## But also...

Note: 
Possible aussi de controler les accès à des APIs externes depuis notre API Gateway

--- 

## API management is a reverse proxy

--- 

## API management is a reverse proxy
### Smart Proxy

Note: 
Plus ou moins vrai

--- 

## API Gateway as an "ESB" ?

---

## API Gateway as an "ESB" ?

Note: 
Complètement opposés

--- 

# API Gateway V.S ESB

* Big part of vendros are comming from SOA/ESB world
* Entrisprise integration vendors have build in API management into their existing solution
* Vendors are seiling...

--- 

# API Gateway V.S ESB

* API Management is not only API Gateway !
* What about publisher / Developer portal ?
* What about analytics ?


--- 

# API Gateway V.S ESB

95% direct access
5% Chained APis

---

# API Gateway V.S ESB
Avoid transofmation, orchestration, ...

--- 

# Gravitee.io

---

# Gravitee.io

* OSS
* Production ready
* Scalable
* API gateway, access management, security, ...

Note: 
* Possibilité d'étendre le fonctionnement par développement de plugins


